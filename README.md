[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/jogos-casuais/palavras-cruzadas)

# Palavras cruzadas

Palavras cruzadas para exercitar o cérebro

Projeto em HTML, CSS e Javascript, utilizando canvas.

## Como desenvolver aqui
1. Na pasta raiz do repositório, rodar `npm i` para instalar as dependências
2. Para testar num server local, rodar `npm test` (o browser deve abrir automaticamente, caso contrário verificar output do terminal)

- [x] Dicionário
  - [x] Com regexp
  - [ ] Ranking de palavras (comuns, incomuns, pouco usadas)
- [ ] Construtor do puzzle
  - [ ] Estrutura de dados
  - [ ] Visual (HTML Canvas)