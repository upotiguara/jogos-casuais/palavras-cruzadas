document.addEventListener("DOMContentLoaded", function(){
    document.getElementById("regex").oninput = function() {
        processInput();
    }
    document.getElementById("ammount").oninput = function() {
        processInput();
    }
    processInput();
  });

function processInput(){
    const input = document.querySelector("#regex").value;
    const ammount = document.querySelector("#ammount").value;
    getWord(input,ammount);
}

function getWord(regexStr,ammount){
    
    document.getElementById('embaralho').innerHTML = "";
    fetch('wordlist/pt-br.txt')
    .then(response => response.text())
    .then(text => {
        const wordListArray = text.split("\n");
        let regex = new RegExp(regexStr);
        console.log('Expressão regular: ' + regexStr);
        console.log(regex);
        const matches = wordListArray.filter(value => regex.test(value.trim()));
        if (matches == 0) {throw "Quebrou"};
        let cruzada = [];
        console.log('Universo: ' + matches.length);
        let arraySize = matches.length;
        var indexArray = [];
        for(var i=0; i < ammount; i++){
            let index = -1;
            do{
                index = Math.floor(Math.random() * arraySize);
            }
            while(indexArray.includes(index));

            indexArray.push(index);
            cruzada.push(matches[index].trim());
        }
            
        document.getElementById('embaralho').innerHTML = cruzada;
    })
}